package de.logicline.adamos.c8y;

public interface DataManagement {
    void sendData(String type, int value);
}
