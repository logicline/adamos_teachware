package de.logicline.adamos.c8y;

import com.cumulocity.microservice.subscription.service.MicroserviceSubscriptionsService;
import com.cumulocity.model.idtype.GId;
import com.cumulocity.rest.representation.AbstractExtensibleRepresentation;
import com.cumulocity.rest.representation.inventory.ManagedObjectRepresentation;
import com.cumulocity.rest.representation.measurement.MeasurementRepresentation;
import com.cumulocity.sdk.client.Platform;
import com.google.common.collect.ImmutableMap;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DataManagementImpl implements DataManagement {

    @Value("${C8Y_TENANT}")
    private String s;

    @Value("${deviceId}")
    private String deviceId;

    @Autowired
    private Platform platform;

    @Autowired
    private MicroserviceSubscriptionsService service;

    @Override
    public void sendData(String type, int value) {
        MeasurementRepresentation m = new MeasurementRepresentation();
        ManagedObjectRepresentation mo = new ManagedObjectRepresentation();
        mo.setId(GId.asGId(deviceId));
        m.setSource(mo);
        m.setDateTime(DateTime.now());
        m.setType(type);
        AbstractExtensibleRepresentation a = getMeasurement(type, value);
        m.setProperty(type, a);
        service.runForTenant(s, () -> platform.getMeasurementApi().create(m));
    }

    private AbstractExtensibleRepresentation getMeasurement(String unit, Integer value) {
        AbstractExtensibleRepresentation a = new AbstractExtensibleRepresentation();
        a.setProperty(unit, ImmutableMap.of(unit, "", "value", value));
        return a;
    }
}
