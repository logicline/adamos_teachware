package de.logicline.adamos;

import org.springframework.beans.factory.annotation.Value;

public class Config {
    @Value("${BRICKD_HOST:localhost}")
    protected String HOST;
    protected int PORT = 4223;
}
