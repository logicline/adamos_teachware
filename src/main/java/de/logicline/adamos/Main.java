package de.logicline.adamos;


import com.cumulocity.microservice.autoconfigure.MicroserviceApplication;
import de.logicline.adamos.sensor.Sensor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.util.List;


@MicroserviceApplication
public class Main {

    private final Logger logger = LoggerFactory.getLogger(Main.class);

    @Autowired(required = false)
    private List<Sensor> sensors;

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @EventListener
    public void ready(ApplicationReadyEvent event) throws Exception {
        logger.info("Hello");

        if (sensors != null)
            sensors.forEach(sensor -> {
                try {
                    sensor.read();
                } catch (Exception e) {
                    logger.error("Exception reading sensor", e);
                }
            });
    }
}
