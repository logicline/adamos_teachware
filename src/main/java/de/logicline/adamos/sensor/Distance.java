package de.logicline.adamos.sensor;

import com.cumulocity.microservice.subscription.service.MicroserviceSubscriptionsService;
import com.cumulocity.rest.representation.AbstractExtensibleRepresentation;
import com.cumulocity.sdk.client.Platform;
import com.google.common.collect.ImmutableMap;
import com.tinkerforge.BrickletDistanceIR;
import com.tinkerforge.IPConnection;
import de.logicline.adamos.Config;
import de.logicline.adamos.c8y.DataManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Distance extends Config implements Sensor {

    private String UID = "Etz";
    private IPConnection ipcon = new IPConnection(); // Create IP connection
    private BrickletDistanceIR dir = new BrickletDistanceIR(UID, ipcon); // Create device object

    @Value("${C8Y_TENANT}")
    private String s;

    @Value("${deviceId}")
    private String deviceId;

    @Autowired
    private Platform platform;

    @Autowired
    private MicroserviceSubscriptionsService service;

    @Autowired
    private DataManagement m1;

    public void read() throws Exception {

        ipcon.connect(HOST, PORT); // Connect to brickd
        dir.addDistanceListener(new BrickletDistanceIR.DistanceListener() {
            public void distance(int distance) {
                System.out.println("Distance: " + distance / 10.0 + " cm");
                m1.sendData("distance", distance / 10);


            }
        });

        // Set period for distance callback to 0.1s (100ms)
        // Note: The distance callback is only called every 0.1 seconds
        //       if the distance has changed since the last call!
        dir.setDistanceCallbackPeriod(100);
    }

    public void disconnect() throws Exception {
        ipcon.disconnect();
    }

    private AbstractExtensibleRepresentation getMeasurement(String unit, Integer value) {
        AbstractExtensibleRepresentation a = new AbstractExtensibleRepresentation();
        a.setProperty("distance", ImmutableMap.of("unit", "cm", "value", value));
        return a;
    }
}
