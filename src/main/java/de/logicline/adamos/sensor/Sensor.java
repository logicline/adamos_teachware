package de.logicline.adamos.sensor;

import com.tinkerforge.IPConnection;
import org.springframework.beans.factory.annotation.Value;

public interface Sensor {


    void read() throws Exception;

    void disconnect() throws Exception;

}
