package de.logicline.adamos.sensor;

import com.tinkerforge.BrickletLinearPoti;
import com.tinkerforge.IPConnection;
import de.logicline.adamos.Config;
import de.logicline.adamos.c8y.DataManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class LinearPoti extends Config implements Sensor{

    private String UID = "A1w";     //UID of Linear Poti
    private IPConnection ipcon = new IPConnection();    //create IP connection
    private BrickletLinearPoti linearPoti = new BrickletLinearPoti(UID, ipcon);     //create device

    @Autowired
    private DataManagement m1;

    public void read() throws Exception {

        ipcon.connect(HOST, PORT);

        linearPoti.addPositionListener(new BrickletLinearPoti.PositionListener() {
            public void position(int position) {

                System.out.println("Position: " + position);
                m1.sendData("Position", position);

            }
        });
        linearPoti.setPositionCallbackPeriod(100);
    }

    public void disconnect() throws Exception {
        ipcon.disconnect();
    }
}
