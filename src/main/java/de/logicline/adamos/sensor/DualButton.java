package de.logicline.adamos.sensor;

import com.cumulocity.microservice.subscription.service.MicroserviceSubscriptionsService;
import com.cumulocity.rest.representation.AbstractExtensibleRepresentation;
import com.cumulocity.sdk.client.Platform;
import com.google.common.collect.ImmutableMap;
import com.tinkerforge.BrickletDualButton;
import com.tinkerforge.IPConnection;
import de.logicline.adamos.Config;
import de.logicline.adamos.c8y.DataManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DualButton extends Config implements Sensor {

    @Value("${C8Y_TENANT}")
    private String s;

    @Value("${deviceId}")
    private String deviceId;

    @Autowired
    private Platform platform;

    @Autowired
    private MicroserviceSubscriptionsService service;

    @Autowired
    private DataManagement m1;

    private String UID = "vUh";
    private IPConnection ipcon = new IPConnection(); // Create IP connection
    private BrickletDualButton db = new BrickletDualButton(UID, ipcon); // Create device object

    public void read() throws Exception {

        ipcon.connect(HOST, PORT); // Connect to brickd
        // Don't use device before ipcon is connected

        // Add state changed listener
        db.addStateChangedListener(new BrickletDualButton.StateChangedListener() {
            public void stateChanged(short buttonL, short buttonR, short ledL,
                                     short ledR) {
                if (buttonL == BrickletDualButton.BUTTON_STATE_PRESSED) {
                    System.out.println("Left Button: Pressed");
                } else if (buttonL == BrickletDualButton.BUTTON_STATE_RELEASED) {
                    System.out.println("Left Button: Released");
                }

                if (buttonR == BrickletDualButton.BUTTON_STATE_PRESSED) {
                    System.out.println("Right Button: Pressed");
                } else if (buttonR == BrickletDualButton.BUTTON_STATE_RELEASED) {
                    System.out.println("Right Button: Released");
                }

                System.out.println("");

                m1.sendData("Status1", (int) buttonL);
                m1.sendData("Status2", (int) buttonR);
            }
        });
    }

    public void disconnect() throws Exception {
        ipcon.disconnect();
    }

    private AbstractExtensibleRepresentation getMeasurement(String unit, Integer value) {
        AbstractExtensibleRepresentation a = new AbstractExtensibleRepresentation();
        a.setProperty("Status", ImmutableMap.of("unit", "", "value", value));
        return a;
    }
}
