package de.logicline.adamos.c8y;

import com.cumulocity.microservice.subscription.service.MicroserviceSubscriptionsService;
import com.cumulocity.sdk.client.Platform;
import de.logicline.adamos.sensor.Sensor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest (classes = {DataManagementImpl.class}) // TODO add real classes under test here
public class DataManagementImplTest {

    @MockBean
    Sensor sensor;

    @MockBean
    Platform platform;

    @MockBean
    MicroserviceSubscriptionsService service;

    @Test
    public void sendData() {
    }
}
