# Adamos showcase

see: https://toolchain.logicline.de/confluence/display/IoT/Adamos+Teachware+Suitcase

## Startup

You need docker, docker-compose and maven to run this.

Execute:

1. `git clone git@bitbucket.org:logicline/adamos_teachware.git`
2. `cd adamos_teachware`
2. `mvn clean package`
3. `cd docker-compose && docker-compose up -d`